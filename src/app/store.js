import { configureStore } from '@reduxjs/toolkit';
import historyReducer from '../features/history/historySlice';
import meReducer from '../features/info/infoSlice';
import photosReducer from '../features/photos/photosSlice';

const rootReducer = {
  me: meReducer,
  history: historyReducer,
  photos: photosReducer,
};

export default configureStore({
  reducer: rootReducer,
});
