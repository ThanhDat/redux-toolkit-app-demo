import { createSlice, createAsyncThunk } from '@reduxjs/toolkit';
import userAPI from '../../api/me/userAPI';

export const getMe = createAsyncThunk(
  'user/getMe',
  async (params, thunkAPI) => {
    const currentUser = await userAPI.getMe(params);
    return currentUser.data;
  }
);

const initialState = {
  current: {},
  loading: false,
  error: '',
};

export const infoSlice = createSlice({
  name: 'me',
  initialState,
  reducers: {
    setMe: (state, action) => ({ ...initialState, current: action.payload }),
    signOut: () => ({}),
  },
  extraReducers: {
    [getMe.pending]: (state) => {
      state.loading = true;
    },
    [getMe.rejected]: (state, action) => {
      state.loading = false;
      state.error = action.error;
    },
    [getMe.fulfilled]: (state, action) => {
      if (action.payload.err) {
        state.error = action.payload;
      } else {
        state.current = action.payload;
      }
      state.loading = false;
    },
  },
});

export const { setMe, signOut } = infoSlice.actions;

export const selectMe = (state) => state.me.current;

export default infoSlice.reducer;
