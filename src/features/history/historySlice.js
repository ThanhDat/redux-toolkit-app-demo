import { createSlice } from '@reduxjs/toolkit';

export const historySlice = createSlice({
  name: 'history',
  initialState: {},
  reducers: {
    setHistory: (state, action) => action.payload,
  },
});

export const { setHistory } = historySlice.actions;

export const selectHistory = (state) => state.history;

export default historySlice.reducer;
