import { createSlice, createAsyncThunk } from '@reduxjs/toolkit';
import photosAPI from '../../api/photos/photosApi';

export const deletePhoto = createAsyncThunk(
  'photos/deletePhotos',
  async (photoId, thunkAPI) => {
    const currentPhotos = await photosAPI.deletePhoto(photoId);
    return currentPhotos.data;
  }
);

export const createPhoto = createAsyncThunk(
  'photos/createPhotos',
  async (data, thunkAPI) => {
    const currentPhotos = await photosAPI.createPhoto(data);
    return currentPhotos.data;
  }
);

export const updatePhoto = createAsyncThunk(
  'photos/updatePhotos',
  async (data, thunkAPI) => {
    const { id, ...fields } = data;
    const currentPhotos = await photosAPI.updatePhoto(id, fields);
    return currentPhotos.data;
  }
);

export const getPhotos = createAsyncThunk(
  'photos/getPhotos',
  async (params, thunkAPI) => {
    const currentPhotos = await photosAPI.getPhotos(params);
    return currentPhotos.data;
  }
);

export const getPhotoById = createAsyncThunk(
  'photos/getPhotoById',
  async (id, thunkAPI) => {
    const currentPhotos = await photosAPI.getPhotoById(id);
    return currentPhotos.data;
  }
);

const initialState = {
  current: {},
  loading: false,
  error: '',
};

export const photosSlice = createSlice({
  name: 'photos',
  initialState,
  // extraReducers: {

  // }
});

export default photosSlice.reducer;
