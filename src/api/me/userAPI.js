import axiosClient from '../axiosClient';

const url = process.env.REACT_APP_API_URL;

const userAPI = {
  getMe: (data) =>
    axiosClient({
      method: 'POST',
      url: `${url}/api/users/login`,
      data,
    }).catch((err) => err.response),
};

export default userAPI;
