import axiosClient from '../axiosClient';

const url = process.env.REACT_APP_API_URL;

const photosAPI = {
  getPhotos: (params) =>
    axiosClient({
      method: 'GET',
      url: `${url}/api/photos`,
      params,
    }).catch((err) => err.response),
  getPhotoById: (id) =>
    axiosClient({
      method: 'GET',
      url: `${url}/api/photos/${id}`,
    }).catch((err) => err.response),
  deletePhoto: (id) =>
    axiosClient({
      method: 'DELETE',
      url: `${url}/api/photos/${id}`,
    }).catch((err) => {
      throw new Error(err);
    }),
  createPhoto: (data) =>
    axiosClient({
      method: 'POST',
      url: `${url}/api/photos/`,
      data,
    }).catch((err) => {
      throw new Error(err);
    }),
  updatePhoto: (id, data) =>
    axiosClient({
      method: 'PUT',
      url: `${url}/api/photos/${id}`,
      data,
    }).catch((err) => {
      throw new Error(err);
    }),
};

export default photosAPI;
