import axios from 'axios';
import Cookies from 'universal-cookie';

const cookies = new Cookies();

// Set up default config for http requests here
// Please have a look at here `https://github.com/axios/axios#request-config` for the full list of configs
const axiosClient = axios.create({
  baseURL: process.env.REACT_APP_API_URL,
  headers: {
    'content-type': 'application/json',
  },
});

// eslint-disable-next-line arrow-body-style
axiosClient.interceptors.request.use(async (config) => {
  const tmp = config;
  if (cookies.get('me') && cookies.get('me').token) {
    tmp.headers.Authorization = `bearer ${cookies.get('me').token}`;
  }
  // Handle token here ...
  return config;
});

// eslint-disable-next-line arrow-body-style
axiosClient.interceptors.response.use(
  (response) =>
    // Muốn axios trả về response.data sẵn thì dùng
    // if (response && response.data) {
    //   return response.data;
    // }
    response,
  (error) => {
    // Handle errors
    throw error;
  }
);

export default axiosClient;
