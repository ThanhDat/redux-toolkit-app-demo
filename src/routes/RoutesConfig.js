import React from 'react';
import { Switch, Route } from 'react-router-dom';
import PhotoDetail from '../components/PhotoDetail';
import FirebaseTest from '../components/FirebaseTest';
import NotFound from '../components/NotFound';
import PhotosNode from '../components/PhotosNode';
import SignInNode from '../components/SignInNode';

function RoutesConfig() {
  return (
    <Switch>
      <Route component={PhotosNode} exact path="/photos" />
      <Route component={PhotoDetail} exact path="/photos/:id" />
      <Route component={PhotoDetail} exact path="/photos/:id/edit" />
      <Route component={PhotoDetail} exact path="/photos/add-photo" />
      <Route component={PhotosNode} exact path="/" />
      <Route component={FirebaseTest} exact path="/firebase" />
      <Route component={SignInNode} exact path="/sign-in" />
      <Route component={NotFound} path="*" />
    </Switch>
  );
}

export default RoutesConfig;
