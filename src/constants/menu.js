import {
  DesktopOutlined,
  FireOutlined,
  UnorderedListOutlined,
} from '@ant-design/icons';

export const menuSideBar = [
  {
    url: '/photos',
    icon: <DesktopOutlined />,
    nameMenu: 'Nodejs Server',
  },
  {
    url: '/firebase',
    icon: <FireOutlined />,
    nameMenu: 'Firebase',
  },
  {
    url: '/hoc',
    icon: <UnorderedListOutlined />,
    nameMenu: 'HOC',
  },
];
