import React, { useState } from 'react';
import { Link, useLocation, useHistory } from 'react-router-dom';
import { Layout, Menu } from 'antd';
import {
  DesktopOutlined,
  FileImageOutlined,
  UserOutlined,
  FireOutlined,
  LoginOutlined,
  LogoutOutlined,
} from '@ant-design/icons';
import firebase from 'firebase/app';
import 'firebase/auth';

const { Sider } = Layout;
const { SubMenu } = Menu;
function Sidebar() {
  const [collapsed, setCollapsed] = useState(false);
  const [signedIn, setSignedIn] = useState(false);
  const [displayName, setDisplayName] = useState('');
  const location = useLocation();
  const history = useHistory();
  const pathname = location.pathname === '/' ? '/photos' : location.pathname;
  firebase.auth().onAuthStateChanged((user) => {
    if (user) {
      // User is signed in.
      setSignedIn(true);
      setDisplayName(user.displayName);
    } else if (pathname !== '/sign-in' && pathname !== '/sign-in-node') {
      // No user is signed in.
      setSignedIn(false);
      history.push({
        pathname: '/sign-in-node',
      });
    }
  });

  function onCollapse() {
    setCollapsed(!collapsed);
  }

  function handleSignOut() {
    firebase
      .auth()
      .signOut()
      .then(() => {
        // Sign-out successful.
        setSignedIn(false);
      })
      .catch((error) => {
        // An error happened.
        console.log('Error sign out', error);
      });
  }

  return (
    <Sider collapsed={collapsed} collapsible onCollapse={onCollapse}>
      <div className="logo" />
      <Menu mode="inline" selectedKeys={pathname} theme="dark">
        {signedIn ? (
          <>
            <Menu.Item key="/photos" icon={<FileImageOutlined />}>
              <Link to="/photos">JSON Server</Link>
            </Menu.Item>
            <Menu.Item key="/nodejs" icon={<DesktopOutlined />}>
              <Link to="/photos-node">Nodejs Server</Link>
            </Menu.Item>
            <Menu.Item key="/firebase" icon={<FireOutlined />}>
              <Link to="/firebase">Firebase</Link>
            </Menu.Item>
            <SubMenu key="sub1" icon={<UserOutlined />} title={displayName}>
              <Menu.Item
                key="10"
                icon={<LogoutOutlined />}
                onClick={handleSignOut}
              >
                Sign Out
              </Menu.Item>
            </SubMenu>
          </>
        ) : (
          <>
            <Menu.Item key="11" icon={<LoginOutlined />}>
              <Link to="/sign-in">Sign In</Link>
            </Menu.Item>
            <Menu.Item key="12" icon={<LoginOutlined />}>
              <Link to="/sign-innode">Sign In Node</Link>
            </Menu.Item>
          </>
        )}
      </Menu>
    </Sider>
  );
}

export default Sidebar;
