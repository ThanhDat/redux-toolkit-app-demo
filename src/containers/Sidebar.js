import React, { useState, useEffect } from 'react';
import { Link, useLocation } from 'react-router-dom';
import { useDispatch } from 'react-redux';
import { Layout, Menu } from 'antd';
import { UserOutlined, LoginOutlined, LogoutOutlined } from '@ant-design/icons';
import { isEmpty } from 'lodash';
import PropTypes from 'prop-types';
import Cookies from 'universal-cookie';
import { signOut } from '../features/info/infoSlice';
import { menuSideBar } from '../constants/menu';

const { Sider } = Layout;
const { SubMenu } = Menu;
function Sidebar(props) {
  const { user } = props;
  const [collapsed, setCollapsed] = useState(false);
  const [displayName, setDisplayName] = useState('');
  const location = useLocation();
  const dispatch = useDispatch();
  const cookies = new Cookies();
  const pathname = location.pathname === '/' ? '/photos' : location.pathname;

  function handleSignOut() {
    dispatch(signOut());
    cookies.remove('me');
  }

  function renderSideBar() {
    const sideBar = menuSideBar.map((obj) => {
      if (!obj.subMenu) {
        return (
          <Menu.Item key={obj.url} icon={obj.icon}>
            <Link to={obj.url}>{obj.nameMenu}</Link>
          </Menu.Item>
        );
      }
      return (
        <SubMenu key={obj.url} icon={obj.icon} title={displayName}>
          {obj.subMenu.map((subMenu) => (
            <Menu.Item key={subMenu.key} icon={subMenu.url}>
              {subMenu.nameMenu}
            </Menu.Item>
          ))}
        </SubMenu>
      );
    });
    sideBar.push(
      <SubMenu key="displayName" icon={<UserOutlined />} title={displayName}>
        <Menu.Item
          key="sign-out"
          icon={<LogoutOutlined />}
          onClick={handleSignOut}
        >
          Sign Out
        </Menu.Item>
      </SubMenu>
    );
    return sideBar;
  }

  useEffect(() => {
    renderSideBar();
  }, []);

  useEffect(() => {
    if (!isEmpty(user)) {
      setDisplayName(`${user.firstName} ${user.lastName}`);
    }
  }, [user]);

  function onCollapse() {
    setCollapsed(!collapsed);
  }

  return (
    <Sider
      collapsed={collapsed}
      collapsible
      onCollapse={onCollapse}
      style={{
        overflow: 'auto',
        height: '100vh',
        position: 'fixed',
        left: 0,
      }}
    >
      <div className="logo" />
      <Menu mode="inline" selectedKeys={pathname} theme="dark">
        {!isEmpty(user) ? (
          <>
            {/* <Menu.Item key="/photos" icon={<DesktopOutlined />}>
              <Link to="/photos">Nodejs Server</Link>
            </Menu.Item>
            <Menu.Item key="/firebase" icon={<FireOutlined />}>
              <Link to="/firebase">Firebase</Link>
            </Menu.Item>
            <SubMenu key="sub1" icon={<UserOutlined />} title={displayName}>
              <Menu.Item
                key="10"
                icon={<LogoutOutlined />}
                onClick={handleSignOut}
              >
                Sign Out
              </Menu.Item>
            </SubMenu> */}
            {renderSideBar()}
          </>
        ) : (
          <>
            {/* <Menu.Item key="11" icon={<LoginOutlined />}>
              <Link to="/sign-in">Sign In</Link>
            </Menu.Item> */}
            <Menu.Item key="/sign-in" icon={<LoginOutlined />}>
              <Link to="/sign-in">Sign In Node</Link>
            </Menu.Item>
          </>
        )}
      </Menu>
    </Sider>
  );
}

Sidebar.propTypes = {
  user: PropTypes.objectOf(PropTypes.any),
};

export default Sidebar;
