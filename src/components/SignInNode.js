import React, { useState } from 'react';
import { useDispatch } from 'react-redux';
import { Form, Input, Button } from 'antd';
import Cookies from 'universal-cookie';
import { unwrapResult } from '@reduxjs/toolkit';
import { isString } from 'lodash';
import { getMe } from '../features/info/infoSlice';

function SignInNode() {
  const dispatch = useDispatch();
  const [msg, setMsg] = useState('');
  const [loading, setLoading] = useState(false);
  const cookies = new Cookies();
  const layout = {
    labelCol: { span: 8 },
    wrapperCol: { span: 8 },
  };
  const tailLayout = {
    wrapperCol: { offset: 8, span: 16 },
  };

  async function onFinish(data) {
    try {
      setLoading(true);
      const actionResult = await dispatch(getMe(data));
      const currentUser = unwrapResult(actionResult);
      if (currentUser.err) {
        if (isString(currentUser.err)) {
          throw new Error(currentUser.err);
        }
        throw currentUser.err;
      }
      cookies.set('me', currentUser);
    } catch (error) {
      setMsg('Username or password is invalid2');
    } finally {
      setLoading(false);
    }
  }

  function onFinishFailed(errorInfo) {
    console.log('🚀 ~ errorInfo', errorInfo);
  }

  return (
    <Form
      {...layout}
      name="basic"
      onFinish={onFinish}
      onFinishFailed={onFinishFailed}
    >
      <Form.Item
        label="Email"
        name="email"
        rules={[{ required: true, message: 'Please input your email!' }]}
      >
        <Input />
      </Form.Item>
      <Form.Item
        label="Password"
        name="password"
        rules={[{ required: true, message: 'Please input your password!' }]}
      >
        <Input.Password />
      </Form.Item>
      <Form.Item {...tailLayout}>
        <Button htmlType="submit" loading={loading} type="primary">
          Submit
        </Button>
        <h1>{msg}</h1>
      </Form.Item>
    </Form>
  );
}

export default SignInNode;
