import React, { useState, useEffect } from 'react';
import { Button, Input, Alert, Skeleton, notification } from 'antd';
import { useHistory, useRouteMatch } from 'react-router-dom';
import axiosClient from '../api/axiosClient';

function PhotoDetail() {
  const history = useHistory();
  const [data, setData] = useState([]);
  const [isLoading, setIsLoading] = useState(false);
  const [isError, setIsError] = useState(false);
  const match = useRouteMatch();
  const { id } = match.params;

  useEffect(() => {
    const fetchData = async () => {
      setIsError(false);
      setIsLoading(true);
      try {
        const result = await axiosClient({
          method: 'GET',
          url: `http://localhost:3004/photos/${id}`,
        }).catch((err) => err.response);
        setData(result.data);
      } catch (error) {
        setIsError(true);
      }
      setIsLoading(false);
    };
    fetchData();
  }, [id]);

  function onChangeTitle(e) {
    setData({ ...data, title: e.target.value });
  }

  async function onSave() {
    setIsLoading(true);
    await axiosClient({
      method: 'PUT',
      url: `http://localhost:3004/photos/${id}`,
      data,
    })
      .then((res) => {
        notification.success({
          message: 'Thông báo',
          description: 'Edit thành công',
        });
        return res.response;
      })
      .catch((err) => {
        notification.error({
          message: 'Thông báo',
          description: 'Edit thất bại',
        });
        return err.response;
      });
    setIsLoading(false);
  }

  function onGoBack() {
    history.push('/photos');
  }

  return (
    <Skeleton active loading={isLoading}>
      {!isError ? (
        <>
          <Button onClick={onGoBack}>Back</Button>
          <hr />
          <div>
            <p>
              <img alt="" src={data.thumbnailUrl} />
            </p>
            <p>{data.title}</p>
            <Input
              onChange={onChangeTitle}
              placeholder="Basic usage"
              value={data.title}
            />
            <Button onClick={onSave}>Save</Button>
          </div>
        </>
      ) : (
        <Alert
          description="Vui lòng thử lại sau."
          message="Có lỗi xảy ra!"
          showIcon
          type="error"
        />
      )}
    </Skeleton>
  );
}

export default PhotoDetail;
