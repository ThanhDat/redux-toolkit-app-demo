import React, { useEffect, useState, useRef } from 'react';
import { Layout, Breadcrumb, Skeleton } from 'antd';
import firebase from 'firebase/app';
import { useSelector, useDispatch } from 'react-redux';
import { isEmpty } from 'lodash';
import { useHistory, useLocation } from 'react-router-dom';
import Cookies from 'universal-cookie';
import Sidebar from '../containers/Sidebar';
import RoutesConfig from '../routes/RoutesConfig';
import config from '../firebase/config';
import { selectMe, setMe } from '../features/info/infoSlice';

const { Header, Content, Footer } = Layout;

function MainApp() {
  if (!firebase.apps.length) {
    firebase.initializeApp(config);
  }
  const cookies = new Cookies();
  const history = useHistory();
  const [loading, setLoading] = useState(false);
  const me = useSelector(selectMe);
  const info = cookies.get('me') || me;
  const location = useLocation();
  const dispatch = useDispatch();
  const isFirstRun = useRef(true);
  const [isRemoveLS, setRemoveLS] = useState(false);

  const pathname =
    location.pathname === '/' ? '/photos-node' : location.pathname;

  useEffect(() => {
    if (isFirstRun.current) {
      localStorage.removeItem('dataFilter');
      setLoading(true);
      const getInfo = async () => {
        if (!isEmpty(info)) {
          dispatch(setMe(info));
        }
      };
      getInfo();
      setRemoveLS(true);
      isFirstRun.current = false;
    }
  }, [dispatch, info]);

  useEffect(() => {
    if (!isFirstRun.current) {
      if (isEmpty(info)) {
        if (pathname !== '/sign-in' && pathname !== '/sign-in-node') {
          history.push({
            pathname: '/sign-in',
          });
        }
      } else if (pathname === '/sign-in' || pathname === '/sign-in-node') {
        history.goBack();
      }
      setLoading(false);
    }
  }, [history, info, pathname]);

  return (
    <Layout style={{ minHeight: '100vh' }}>
      <Sidebar user={me} />
      {isRemoveLS && (
        <Skeleton loading={loading} style={{ margin: 'auto' }}>
          <Layout className="site-layout" style={{ marginLeft: 200 }}>
            <Header className="site-layout-background" style={{ padding: 0 }} />
            <Content style={{ margin: '0 16px' }}>
              <Breadcrumb style={{ margin: '16px 0' }}>
                <Breadcrumb.Item>User</Breadcrumb.Item>
                <Breadcrumb.Item>Bill</Breadcrumb.Item>
              </Breadcrumb>
              <div
                className="site-layout-background"
                style={{ padding: 24, minHeight: 360 }}
              >
                <RoutesConfig />
              </div>
            </Content>
            <Footer style={{ textAlign: 'center' }}>
              Ant Design ©2018 Created by Ant UED
            </Footer>
          </Layout>
        </Skeleton>
      )}
    </Layout>
  );
}

export default MainApp;
