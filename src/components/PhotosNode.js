import React, { useState, useEffect, useRef, useMemo } from 'react';
import { notification, Tag, Table, Alert, Button, Modal } from 'antd';
import { PlusOutlined, StarFilled, StarOutlined } from '@ant-design/icons';
import { useHistory } from 'react-router-dom';
import { useDispatch } from 'react-redux';
import { isEmpty } from 'lodash';
import { unwrapResult } from '@reduxjs/toolkit';
import { deletePhoto, getPhotos } from '../features/photos/photosSlice';
import HOC from './HOC';

function PhotosNode() {
  const [data, setData] = useState([]);
  const [total, setTotal] = useState(0);
  const lsData = localStorage.getItem('dataFilter');
  const dataFilter = useMemo(() => {
    const result = !isEmpty(lsData)
      ? JSON.parse(lsData)
      : { page: 1, perPage: 5 };
    return result;
  }, [lsData]);
  const [params, setParams] = useState(dataFilter);
  const [isLoading, setIsLoading] = useState(false);
  const [isError, setIsError] = useState(false);
  const history = useHistory();
  const dispatch = useDispatch();
  const isFirstRun = useRef(true);

  useEffect(() => {
    if (isFirstRun.current) {
      const getLocalData = async () => {
        if (!isEmpty(dataFilter)) {
          setParams(dataFilter);
        }
      };
      isFirstRun.current = false;
      getLocalData();
    }
  }, [dataFilter]);

  useEffect(() => {
    if (!isFirstRun.current) {
      const fetchData = async () => {
        setIsError(false);
        setIsLoading(true);
        try {
          const actionResult = await dispatch(getPhotos(params));
          const currentPhotos = unwrapResult(actionResult);
          setTotal(currentPhotos.totalDocs);
          setData(currentPhotos.docs);
        } catch (error) {
          setIsError(true);
        } finally {
          setIsLoading(false);
        }
      };
      fetchData();
    }
  }, [dispatch, params]);

  function handleTableChange(pagination) {
    setParams({ page: pagination.current, perPage: params.perPage });
  }

  function handleRedirect(record) {
    localStorage.setItem('dataFilter', JSON.stringify(params));
    history.push({
      pathname: `/photos/${record._id}`,
    });
  }

  function onClickEdit(record, e) {
    e.stopPropagation();
    localStorage.setItem('dataFilter', JSON.stringify(params));
    history.push({
      pathname: `/photos/${record._id}/edit`,
    });
  }

  function onClickDelete(record, e) {
    e.stopPropagation();
    Modal.confirm({
      type: 'confirm',
      title: 'Confirm Delete',
      content: 'Are you sure you want to delete this item?',
      onOk: async () => {
        setIsLoading(true);
        try {
          const actionResult = await dispatch(deletePhoto(record._id));
          const currentPhotos = unwrapResult(actionResult);
          notification.success({
            message: 'Thông báo',
            description: 'Xóa thành công',
          });
          setParams({ ...params });
        } catch (err) {
          notification.error({
            message: 'Thông báo',
            description: 'Xóa thất bại',
          });
        } finally {
          setIsLoading(false);
        }
      },
    });
  }

  function renderRating(star = 0) {
    const result = [];
    for (let index = 0; index < star; index += 1) {
      result.push(<StarFilled key={index + 1} />);
    }
    for (let index = star; index < 5; index += 1) {
      result.push(<StarOutlined key={index + 1} />);
    }
    return result;
  }

  function handleAddNew() {
    history.push({
      pathname: `/photos/add-photo`,
    });
  }

  const columns = [
    {
      title: 'Thumbnail',
      dataIndex: 'url',
      key: 'url',
      render: (row) => ({
        children: <img alt="" src={row} style={{ height: '30px' }} />,
      }),
    },
    {
      title: 'Name',
      dataIndex: 'title',
      key: 'title',
    },
    {
      title: 'Rating',
      dataIndex: 'rating',
      key: 'rating',
      render: (row) => ({
        children: renderRating(row),
      }),
    },
    {
      title: 'Action',
      // eslint-disable-next-line react/display-name
      render: (record) => (
        <>
          <Tag
            color="geekblue"
            onClick={(e) => onClickEdit(record, e)}
            style={{ cursor: 'pointer' }}
          >
            EDIT
          </Tag>
          <Tag
            color="volcano"
            onClick={(e) => onClickDelete(record, e)}
            style={{ cursor: 'pointer' }}
          >
            DELETE
          </Tag>
        </>
      ),
    },
  ];

  return !isError ? (
    <>
      <div style={{ marginBottom: 16 }}>
        <Button
          icon={<PlusOutlined />}
          onClick={handleAddNew}
          size="large"
          type="primary"
        >
          Add New
        </Button>
      </div>
      <Table
        columns={columns}
        dataSource={data}
        loading={isLoading}
        onChange={handleTableChange}
        onRow={(record) => ({
          onClick: () => {
            handleRedirect(record);
          },
        })}
        pagination={{
          pageSize: params.perPage,
          current: params.page,
          total,
          showTotal: (t) =>
            params.perPage <= total
              ? `Show ${params.perPage} in ${t}`
              : `Total ${t}`,
          showSizeChanger: false,
          showQuickJumper: true,
        }}
        rowKey={(record) => record._id}
      />
    </>
  ) : (
    <Alert
      description="Vui lòng thử lại sau."
      message="Có lỗi xảy ra!"
      showIcon
      type="error"
      zz="haha"
    />
  );
}

// test Higher-Order Component
const WrappedComponent = HOC(PhotosNode);
export default WrappedComponent;
