import React from 'react';

const withStorage = (WrappedComponent) => {
  const injectedProp = {
    name: 'test',
    value: 123,
  };
  function NotFound(props) {
    return <WrappedComponent {...props} injectedProp={injectedProp} />;
  }
  return NotFound;
};

export default withStorage;
