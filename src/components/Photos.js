import React, { useState, useEffect } from 'react';
import { notification, Tag, Table, Alert } from 'antd';
import { useHistory } from 'react-router-dom';
import axiosClient from '../api/axiosClient';

function Photos() {
  const [data, setData] = useState([]);
  const [pageNumber, setPageNumber] = useState(1);
  const [pageSize] = useState(10);
  const [total, setTotal] = useState(0);
  const [params, setParams] = useState({ _page: 1, _limit: 10 });
  const [isLoading, setIsLoading] = useState(false);
  const [isError, setIsError] = useState(false);
  const history = useHistory();

  useEffect(() => {
    const fetchData = async () => {
      setIsError(false);
      setIsLoading(true);
      try {
        const result = await axiosClient({
          method: 'GET',
          url: 'http://localhost:3004/photos',
          params,
        }).catch((err) => err.response);
        setTotal(result.headers['x-total-count']);
        setData(result.data);
      } catch (error) {
        setIsError(true);
      }
      setIsLoading(false);
    };
    fetchData();
  }, [params]);

  function handleTableChange(pagination) {
    setPageNumber(pagination.current);
    setParams({ _page: pagination.current, _limit: pageSize });
  }

  function onClickEdit(record, e) {
    e.stopPropagation();
    history.push({
      pathname: `/photos/${record.id}`,
    });
  }

  async function onClickDelete(record, e) {
    e.stopPropagation();
    setIsLoading(true);
    await axiosClient({
      method: 'DELETE',
      url: `http://localhost:3004/photos/${record.id}`,
    })
      .then((res) => {
        notification.success({
          message: 'Thông báo',
          description: 'Xóa thành công',
        });
        return res.response;
      })
      .catch((err) => {
        notification.error({
          message: 'Thông báo',
          description: 'Xóa thất bại',
        });
        return err.response;
      });
    setIsLoading(false);
    setParams({ ...params });
  }

  const columns = [
    {
      title: 'Thumbnail',
      dataIndex: 'thumbnailUrl',
      key: 'thumbnailUrl',
      render: (row) => ({
        children: <img alt="" src={row} style={{ height: '30px' }} />,
      }),
    },
    {
      title: 'Name',
      dataIndex: 'title',
      key: 'title',
    },
    {
      title: 'Action',
      // eslint-disable-next-line react/display-name
      render: (record) => (
        <>
          <Tag color="geekblue" onClick={(e) => onClickEdit(record, e)}>
            EDIT
          </Tag>
          <Tag color="volcano" onClick={(e) => onClickDelete(record, e)}>
            DELETE
          </Tag>
        </>
      ),
    },
  ];

  return !isError ? (
    <Table
      columns={columns}
      dataSource={data}
      loading={isLoading}
      onChange={handleTableChange}
      pagination={{
        pageSize,
        current: pageNumber,
        total,
        showTotal: (t) =>
          pageSize <= total
            ? `Hiển thị ${pageSize} trong tổng ${t}`
            : `Tổng ${t}`,
        showSizeChanger: false,
        showQuickJumper: true,
      }}
      rowKey={(record) => record.id}
    />
  ) : (
    <Alert
      description="Vui lòng thử lại sau."
      message="Có lỗi xảy ra!"
      showIcon
      type="error"
      zz="haha"
    />
  );
}

export default Photos;
