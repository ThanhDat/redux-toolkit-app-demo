import React, { useState, useEffect } from 'react';
import { Tag, Table, Input, Button, notification, Modal } from 'antd';
import firebase from 'firebase/app';
import 'firebase/database';

function FirebaseTest() {
  // eslint-disable-next-line global-require
  const [developers, setDevelopers] = useState([]);
  const [name, setName] = useState('');
  const [role, setRole] = useState('');
  const [uid, setUid] = useState('');
  const [isRefreshed, setIsRefreshed] = useState(false);
  const [isLoading, setIsLoading] = useState(false);

  function writeUserData() {
    firebase
      .database()
      .ref('User/')
      .set(developers, (error) => {
        if (error) {
          // The write failed...
          notification.error({
            message: 'Thông báo',
            description: 'Cập nhật thất bại',
          });
        } else {
          // Data saved successfully!
          notification.success({
            message: 'Thông báo',
            description: 'Cập nhật thành công',
          });
        }
      });
    setIsRefreshed(false);
  }

  function getUserData() {
    setIsLoading(true);
    const ref = firebase.database().ref('User/');
    ref.on(
      'value',
      (snapshot) => {
        const state = snapshot.val();
        if (state) {
          const newState = state.map((obj, index) => {
            const tmp = obj;
            tmp.id = index;
            return tmp;
          });
          setDevelopers(newState);
        }
        setIsLoading(false);
      },
      (error) => {
        console.error(error);
      }
    );
  }

  useEffect(() => {
    getUserData();
    if (isRefreshed) {
      writeUserData();
    }
  }, [isRefreshed]);

  function handleSubmit(e) {
    e.preventDefault();
    if (uid && name && role) {
      const devIndex = developers.findIndex((data) => data.uid === uid);
      developers[devIndex].name = name;
      developers[devIndex].role = role;
      setDevelopers(developers);
    } else if (name && role) {
      const newUid = new Date().getTime().toString();
      developers.push({ uid: newUid, name, role });
      setDevelopers(developers);
    }
    setIsRefreshed(true);
    setName('');
    setRole('');
    setUid('');
  }

  function onChangeName(e) {
    setName(e.target.value);
  }

  function onChangeRole(e) {
    setRole(e.target.value);
  }

  function removeData(developer, e) {
    e.stopPropagation();
    Modal.confirm({
      type: 'confirm',
      title: 'Confirm',
      onOk: () => {
        const userRef = firebase.database().ref(`User/${developer.id}`);
        userRef.remove();
        const newState = developers.filter(
          (data) => data.uid !== developer.uid
        );
        setIsRefreshed(true);
        setDevelopers(newState);
      },
      onCancel: () => {},
    });
  }

  function updateData(developer, e) {
    e.stopPropagation();
    setUid(developer.uid);
    setName(developer.name);
    setRole(developer.role);
  }

  const columns = [
    {
      title: 'Name',
      dataIndex: 'name',
      key: 'name',
    },
    {
      title: 'Role',
      dataIndex: 'role',
      key: 'role',
    },
    {
      title: 'Action',
      render: (record) => (
        <>
          <Tag
            color="geekblue"
            onClick={(e) => updateData(record, e)}
            style={{ cursor: 'pointer' }}
          >
            EDIT
          </Tag>
          <Tag
            color="volcano"
            onClick={(e) => removeData(record, e)}
            style={{ cursor: 'pointer' }}
          >
            DELETE
          </Tag>
        </>
      ),
    },
  ];

  return (
    <>
      <div className="container">
        <Table
          columns={columns}
          dataSource={developers}
          loading={isLoading}
          rowKey={(record) => record.uid}
        />
        <div className="row">
          <div className="col-xl-12">
            <h1>Add new team member here</h1>
            <form onSubmit={handleSubmit}>
              <div className="form-row">
                <Input type="hidden" value={uid} />
                <div className="form-group col-md-6">
                  <label>Name</label>
                  <Input
                    className="form-control"
                    onChange={onChangeName}
                    placeholder="Name"
                    type="text"
                    value={name}
                  />
                </div>
                <div className="form-group col-md-6">
                  <label>Role</label>
                  <Input
                    className="form-control"
                    onChange={onChangeRole}
                    placeholder="Role"
                    type="text"
                    value={role}
                  />
                </div>
              </div>
              <br />
              <Button
                className="btn btn-primary"
                onClick={handleSubmit}
                type="primary"
              >
                Save
              </Button>
              <button style={{ display: 'none' }} type="submit" />
            </form>
          </div>
        </div>
        <div className="row">
          <div className="col-xl-12">
            <h3>
              Tutorial{' '}
              <a href="https://www.educative.io/edpresso/firebase-as-simple-database-to-react-app">
                here
              </a>
            </h3>
          </div>
        </div>
      </div>
    </>
  );
}

export default FirebaseTest;
