import React, { useState, useEffect } from 'react';
import { Button, Input, Alert, Skeleton, notification, Form } from 'antd';
import { useHistory, useRouteMatch } from 'react-router-dom';
import {
  StarFilled,
  StarOutlined,
  ArrowLeftOutlined,
  SaveOutlined,
} from '@ant-design/icons';
import { useDispatch } from 'react-redux';
import { unwrapResult } from '@reduxjs/toolkit';
import axiosClient from '../api/axiosClient';
import {
  createPhoto,
  getPhotoById,
  updatePhoto,
} from '../features/photos/photosSlice';

function PhotoDetail() {
  const history = useHistory();
  const dispatch = useDispatch();
  const [data, setData] = useState([]);
  const [artist, setArtist] = useState({
    firstName: '',
    lastName: '',
  });
  const [title, setTitle] = useState('');
  const [isLoading, setIsLoading] = useState(false);
  const [btnLoading, setBtnLoading] = useState(false);
  const [isError, setIsError] = useState(false);
  const match = useRouteMatch();
  const {
    params: { id },
    url,
  } = match;
  let isEdit = false;
  let isAddNew = false;
  if (url.lastIndexOf('/edit') !== -1) {
    // EDIT
    isEdit = true;
  } else if (url.lastIndexOf('/add-photo') !== -1) {
    // ADD NEW
    isEdit = true;
    isAddNew = true;
  } else {
    // VIEW
    isEdit = false;
  }
  const layout = {
    labelCol: { span: 1 },
    wrapperCol: { offset: 1, span: 16 },
  };

  useEffect(() => {
    if (!isAddNew) {
      const fetchData = async () => {
        setIsError(false);
        setIsLoading(true);
        try {
          const actionResult = await dispatch(getPhotoById(id));
          const currentPhoto = unwrapResult(actionResult);
          setData({
            title: currentPhoto.title,
            url: currentPhoto.url,
            rating: currentPhoto.rating,
          });
          setArtist(currentPhoto.artist);
          setTitle(currentPhoto.title);
        } catch (error) {
          setIsError(true);
        }
        setIsLoading(false);
      };
      fetchData();
    }
  }, [dispatch, id, isAddNew]);

  function onChangeData(e) {
    setData({ ...data, [e.target.name]: e.target.value });
  }

  async function onSave() {
    try {
      setBtnLoading(true);
      if (isAddNew) {
        const actionResult = await dispatch(createPhoto(data));
        const currentPhotos = unwrapResult(actionResult);
        notification.success({
          message: 'Notification',
          description: 'Created Completed',
        });
      } else {
        console.log('🚀 ~ data', data);
        const actionResult = await dispatch(updatePhoto({ id, ...data }));
        const currentPhotos = unwrapResult(actionResult);
        notification.success({
          message: 'Notification',
          description: 'Created Completed',
        });
      }
    } catch {
      notification.error({
        message: 'Notification',
        description: 'Edit Failed',
      });
    } finally {
      setBtnLoading(false);
    }
  }

  function onGoBack() {
    history.push('/photos');
  }

  async function changeRating(value) {
    try {
      await axiosClient({
        method: 'PUT',
        url: `http://localhost:3000/api/photos/${id}`,
        data: { rating: value },
      })
        .then((res) => {
          notification.success({
            message: 'Notification',
            description: 'Rating Completed',
          });
          setData({
            title: res.data.title,
            url: res.data.url,
            rating: res.data.rating,
          });
          return res.response;
        })
        .catch((err) => {
          notification.error({
            message: 'Notification',
            description: 'Rating Failed',
          });
          return err.response;
        });
    } catch (err) {
      console.log('err', err);
    }
  }

  function renderRating(star = 0) {
    const result = [];
    for (let index = 0; index < star; index += 1) {
      result.push(
        <StarFilled
          key={index + 1}
          onClick={() => changeRating(index + 1)}
          style={{ cursor: 'pointer' }}
        />
      );
    }
    for (let index = star; index < 5; index += 1) {
      result.push(
        <StarOutlined
          key={index + 1}
          onClick={() => changeRating(index + 1)}
          style={{ cursor: 'pointer' }}
        />
      );
    }
    return result;
  }

  return (
    <Skeleton active loading={isLoading}>
      {!isError ? (
        <>
          <div style={{ marginBottom: 16 }}>
            <Button
              icon={<ArrowLeftOutlined />}
              onClick={onGoBack}
              size="large"
              type="primary"
            >
              Back
            </Button>
          </div>
          <div>
            <p>
              <img alt="" src={data.url} />
            </p>
            {isEdit ? (
              <>
                <Form
                  {...layout}
                  initialValues={{ remember: true }}
                  name="basic"
                >
                  <Form.Item
                    label="Title"
                    rules={[
                      { required: true, message: 'Please input your title!' },
                    ]}
                  >
                    <Input
                      name="title"
                      onChange={onChangeData}
                      placeholder="Type your title here!"
                      value={data.title}
                    />
                  </Form.Item>
                  <Form.Item
                    label="URL"
                    rules={[
                      { required: true, message: 'Please input your url!' },
                    ]}
                  >
                    <Input
                      name="url"
                      onChange={onChangeData}
                      placeholder="Type your url here"
                      value={data.url}
                    />
                  </Form.Item>
                  {!isAddNew && (
                    <Form.Item label="Rating">
                      {renderRating(data.rating)}
                    </Form.Item>
                  )}
                </Form>
                <div style={{ marginTop: 16 }}>
                  <Button
                    icon={<SaveOutlined />}
                    loading={btnLoading}
                    onClick={onSave}
                    size="large"
                    type="primary"
                  >
                    Save
                  </Button>
                </div>
              </>
            ) : (
              <>
                <h1>Title: {title}</h1>
                <h1>Artist: {`${artist.firstName} ${artist.lastName}`}</h1>
                <h1>Rating: {renderRating(data.rating)}</h1>
              </>
            )}
          </div>
        </>
      ) : (
        <Alert
          description="Vui lòng thử lại sau."
          message="Có lỗi xảy ra!"
          showIcon
          type="error"
        />
      )}
    </Skeleton>
  );
}

export default PhotoDetail;
