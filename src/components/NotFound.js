import React from 'react';

function NotFound() {
  return (
    <>
      <div className="container" style={{ textAlign: 'center' }}>
        <img alt="" src="404.png" />
      </div>
    </>
  );
}

export default NotFound;
